-- EDITULTRA BEGIN DATABASE CONNECTION CONFIG
--  DBTYPE : Sqlite3
--  DBHOST : 
--  DBPORT : 
--  DBUSER : 
--  DBPASS : 
--  DBNAME : D:\Program Files\test_editultra.sqlite
-- EDITULTRA END DATABASE CONNECTION CONFIG

CREATE TABLE `test_editultra`  (
  `tid` integer primary key autoincrement,
  `trans_jnls_no` varchar(20) NOT NULL,
  `trans_code` varchar(16) NOT NULL,
  `trans_desc` varchar(60) NULL,
  `response_code` varchar(6) NULL,
  `trans_amt` decimal(12, 2) NULL,
  `trans_timestamp` datetime(0) NULL
) ;

SELECT * FROM test_editultra;

SELECT * FROM pm_config WHERE key="PASSWORD_INVALID_COUNT_MAX";

select *
from jn_trans_list
where trans_jnls_no='2020052312345678';

select trans_jnls_no,trans_code FROM test_editultra where tid=2;

INSERT INTO test_editultra VALUES ( 1 , '2020062100000001' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 2 , '2020062100000002' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 3 , '2020062100000003' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 4 , '2020062100000004' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 5 , '2020062100000005' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 6 , '2020062100000006' , 'APPC0001' , '交易A' , 0 , 100.00 , '2020-06-21 01:02:03' );
INSERT INTO test_editultra VALUES ( 7 , '2020062100000007' , 'APPC0001' , NULL , NULL , NULL , NULL );

DELETE FROM test_editultra;

SELECT * FROM test_editultr;
