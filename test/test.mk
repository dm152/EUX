CC := gcc
MKDIR := mkdir
RM := rm -rf

DIR_OBJS := objs
DIR_TARGET := target

DIRS := $(DIR_OBJS) $(DIR_TARGET)

#target/hello-makefile.out
TARGET := $(DIR_TARGET/hello-makefile.out
#SRCS = {const.c func.c main.c}
SRCS := $(wildcard *.c)
#OBJS = {const.o func.o main.o}
OBJS := $(SRCS:.c=.o)
#OBJS ={objs/const.o objs/func.o objs/main.o}
OBJS = $(addprefix $(DIR_OBJS)/,$(OBJS))

.PHONY : rebuild clean all

$(TARGET) : $(DIRS) $(OBJS)
    $(CC) -o $@ $(OBJS)
    @echo "Target file => $@"
    

$(DIRS) :
    $(MKDIR) $@
    
$(DIR_OBJS)/%.o : %.c
    ifeq ($(DEBUG),true)
    $(CC) -o $@ -g -c $^
    else
    $(CC) -o $@ -c $^
    endif
    
rebuild : clean all

all : $(TARGET)

clean :
    $(RM) $(DIRS)
