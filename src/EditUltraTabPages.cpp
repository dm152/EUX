#include "framework.h"

HWND		g_hwndTabPages ;
RECT		g_rectTabPages = { 0 } ;
struct TabPage	*g_pnodeCurrentTabPage = NULL ;
HMENU		g_hTabPagePopupMenu ;
HMENU		g_hEditorPopupMenu ;
HMENU		g_hSymbolListPopupMenu ;
HMENU		g_hSymbolTreePopupMenu ;
int		g_nTabsHeight = TABS_HEIGHT_DEFAULT ;

HWND		g_hwndTabCloseButton ;
struct TabPage	*g_pnodeCloseButtonTabPage = NULL ;
/*
HBRUSH		g_brushCurrentTabCloseButton ;
*/
HBRUSH		g_brushTabCloseButton ;

int CreateTabPages( HWND hWnd )
{
	RECT		rectMainClient ;
	// long		lControlStyle ;

	::GetClientRect( hWnd , & rectMainClient );
	memcpy( & g_rectTabPages , & rectMainClient , sizeof(RECT) );
	if( g_bIsFileTreeBarShow == FALSE )
	{
		g_rectTabPages.left = rectMainClient.left ;
		g_rectTabPages.right = rectMainClient.right ;
	}
	else
	{
		g_rectTabPages.left = FILETREEBAR_WIDTH_DEFAULT ;
		g_rectTabPages.right = rectMainClient.right ;
	}
	g_rectTabPages.top = rectMainClient.top ;
	g_rectTabPages.bottom = rectMainClient.bottom ;
	// g_hwndTabPages = ::CreateWindow( WC_TABCONTROL , NULL , WS_CHILD|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|TCS_MULTILINE|TCS_BUTTONS|TCS_FLATBUTTONS|TCS_TOOLTIPS , g_rectTabPages.left , g_rectTabPages.top , g_rectTabPages.right-g_rectTabPages.left , g_rectTabPages.bottom-g_rectTabPages.top , hWnd , NULL , g_hAppInstance , NULL ) ;
	g_hwndTabPages = ::CreateWindow( WC_TABCONTROL , NULL , WS_CHILD|TCS_TOOLTIPS|TCS_FOCUSONBUTTONDOWN , g_rectTabPages.left , g_rectTabPages.top , g_rectTabPages.right-g_rectTabPages.left , g_rectTabPages.bottom-g_rectTabPages.top , hWnd , NULL , g_hAppInstance , NULL ) ;
	if( g_hwndTabPages == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建TabControl控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	/*
	lControlStyle = ::GetWindowLong( g_hwndTabPages , GWL_STYLE ) ;
	lControlStyle &= ~TCS_RAGGEDRIGHT ;
	::SetWindowLong( g_hwndTabPages , GWL_STYLE , lControlStyle );

	lControlStyle = TabCtrl_GetExtendedStyle( g_hwndTabPages ) ;
	lControlStyle &= ~TCS_EX_FLATSEPARATORS ;
	TabCtrl_SetExtendedStyle( g_hwndTabPages , lControlStyle );
	*/
	
	SendMessage( g_hwndTabPages , WM_SETFONT , (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);
	TabCtrl_SetPadding( g_hwndTabPages , 17 , 4 );
	
	::ShowWindow( g_hwndTabPages , SW_SHOW);
	::UpdateWindow( g_hwndTabPages );

	g_hTabPagePopupMenu = ::LoadMenu( g_hAppInstance , MAKEINTRESOURCE(IDR_TABPAGE_POPUPMENU) ) ;
	if( g_hTabPagePopupMenu == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建文件选项卡右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	g_hTabPagePopupMenu = ::GetSubMenu( g_hTabPagePopupMenu , 0 ) ;

	g_hEditorPopupMenu = ::LoadMenu( g_hAppInstance , MAKEINTRESOURCE(IDR_EDITOR_POPUPMENU) ) ;
	if( g_hEditorPopupMenu == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建编辑区右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	g_hEditorPopupMenu = ::GetSubMenu( g_hEditorPopupMenu , 0 ) ;

	g_hSymbolListPopupMenu = ::LoadMenu( g_hAppInstance , MAKEINTRESOURCE(IDR_SYMBOLLIST_POPUPMENU) ) ;
	if( g_hSymbolListPopupMenu == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建函数列表右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	g_hSymbolListPopupMenu = ::GetSubMenu( g_hSymbolListPopupMenu , 0 ) ;

	g_hSymbolTreePopupMenu = ::LoadMenu( g_hAppInstance , MAKEINTRESOURCE(IDR_SYMBOLTREE_POPUPMENU) ) ;
	if( g_hSymbolTreePopupMenu == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建函数列表右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	g_hSymbolTreePopupMenu = ::GetSubMenu( g_hSymbolTreePopupMenu , 0 ) ;

	HFONT hTabCloseButtonFont = ::CreateFont(-20, 0, 0, 0, FW_NORMAL, false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, "Arial"); 

	g_hwndTabCloseButton = ::CreateWindowEx( 0 , "BUTTON" , "X" , WS_CHILD|BS_FLAT|BS_OWNERDRAW , 0 , 0 , 0 , 0 , hWnd , NULL , g_hAppInstance , NULL ) ;
	// g_hwndTabCloseButton = ::CreateWindowEx( 0 , "BUTTON" , "X" , WS_CHILD|BS_OWNERDRAW , 0 , 0 , 0 , 0 , hWnd , NULL , g_hAppInstance , NULL ) ;
	if( g_hwndTabCloseButton == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建TabCloseButton控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	SendMessage( g_hwndTabCloseButton , WM_SETFONT , (WPARAM)hTabCloseButtonFont, 0);

	/*
	g_brushCurrentTabCloseButton = ::CreateSolidBrush( ::GetSysColor(COLOR_HIGHLIGHT) ) ;
	*/
	// g_brushTabCloseButton = ::CreateSolidBrush( ::GetSysColor(COLOR_BTNFACE) ) ;
	g_brushTabCloseButton = ::CreateSolidBrush( ::GetSysColor(COLOR_HIGHLIGHT) ) ;

	::ShowWindow( g_hwndTabCloseButton , SW_HIDE);
	::UpdateWindow( g_hwndTabCloseButton );

	return 0;
}

void AdjustTabPages()
{
	RECT		rectMainClient ;

	::GetClientRect( g_hwndMainWindow , & rectMainClient );

	if( g_bIsFileTreeBarShow == FALSE )
	{
		g_rectFileTreeBar.left = 0 ;
		g_rectFileTreeBar.right = 0 ;
		g_rectFileTreeBar.top = rectMainClient.top + g_nToolBarHeight ;
		g_rectFileTreeBar.bottom = rectMainClient.bottom ;

		g_rectTabPages.left = rectMainClient.left ;
		g_rectTabPages.right = rectMainClient.right ;
		g_rectTabPages.top = rectMainClient.top + g_nToolBarHeight ;
		g_rectTabPages.bottom = rectMainClient.bottom - g_nStatusBarHeight ;
	}
	else
	{
		g_rectFileTreeBar.left = rectMainClient.left ;
		g_rectFileTreeBar.right = rectMainClient.left + g_stEditUltraMainConfig.nFileTreeBarWidth ;
		g_rectFileTreeBar.top = rectMainClient.top + g_nToolBarHeight ;
		g_rectFileTreeBar.bottom = rectMainClient.bottom - g_nStatusBarHeight ;

		g_rectTabPages.left = g_rectFileTreeBar.right + SPLIT_WIDTH ;
		g_rectTabPages.right = rectMainClient.right ;
		g_rectTabPages.top = rectMainClient.top + g_nToolBarHeight ;
		g_rectTabPages.bottom = rectMainClient.bottom - g_nStatusBarHeight ;
	}

	return;
}

void AdjustTabPageBox( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage->hwndSymbolList )
	{
		pnodeTabPage->rectScintilla.left = g_rectTabPages.left + SCINTILLA_MARGIN_LEFT ;
		pnodeTabPage->rectScintilla.right = g_rectTabPages.right - SYMBOLLIST_MARGIN_LEFT - g_stEditUltraMainConfig.nSymbolListWidth - SYMBOLLIST_MARGIN_RIGHT - SCINTILLA_MARGIN_RIGHT ;
		pnodeTabPage->rectScintilla.top = g_rectTabPages.top + g_nTabsHeight + SCINTILLA_MARGIN_TOP ;
		pnodeTabPage->rectScintilla.bottom = g_rectTabPages.bottom - SCINTILLA_MARGIN_BOTTOM ;

		pnodeTabPage->rectSymbolList.left = pnodeTabPage->rectScintilla.right + SPLIT_WIDTH + SYMBOLLIST_MARGIN_LEFT ;
		pnodeTabPage->rectSymbolList.right = g_rectTabPages.right - SYMBOLLIST_MARGIN_RIGHT ;
		pnodeTabPage->rectSymbolList.top = g_rectTabPages.top + g_nTabsHeight + SYMBOLLIST_MARGIN_TOP ;
		pnodeTabPage->rectSymbolList.bottom = g_rectTabPages.bottom - SYMBOLLIST_MARGIN_BOTTOM ;

		pnodeTabPage->rectSymbolTree.left = g_rectTabPages.right ;
		pnodeTabPage->rectSymbolTree.right = g_rectTabPages.right ;
		pnodeTabPage->rectSymbolTree.top = g_rectTabPages.top + g_nTabsHeight ;
		pnodeTabPage->rectSymbolTree.bottom = g_rectTabPages.bottom ;
	}
	else if( pnodeTabPage->hwndSymbolTree )
	{
		pnodeTabPage->rectScintilla.left = g_rectTabPages.left + SCINTILLA_MARGIN_LEFT ;
		pnodeTabPage->rectScintilla.right = g_rectTabPages.right - SYMBOLTREE_MARGIN_LEFT - g_stEditUltraMainConfig.nSymbolTreeWidth - SYMBOLTREE_MARGIN_RIGHT - SCINTILLA_MARGIN_RIGHT ;
		pnodeTabPage->rectScintilla.top = g_rectTabPages.top + g_nTabsHeight + SCINTILLA_MARGIN_TOP ;
		pnodeTabPage->rectScintilla.bottom = g_rectTabPages.bottom - SCINTILLA_MARGIN_BOTTOM ;

		pnodeTabPage->rectSymbolList.left = g_rectTabPages.right ;
		pnodeTabPage->rectSymbolList.right = g_rectTabPages.right ;
		pnodeTabPage->rectSymbolList.top = g_rectTabPages.top + g_nTabsHeight ;
		pnodeTabPage->rectSymbolList.bottom = g_rectTabPages.bottom ;

		pnodeTabPage->rectSymbolTree.left = pnodeTabPage->rectScintilla.right + SPLIT_WIDTH + SYMBOLTREE_MARGIN_LEFT ;
		pnodeTabPage->rectSymbolTree.right = g_rectTabPages.right - SYMBOLTREE_MARGIN_RIGHT ;
		pnodeTabPage->rectSymbolTree.top = g_rectTabPages.top + g_nTabsHeight + SYMBOLTREE_MARGIN_TOP ;
		pnodeTabPage->rectSymbolTree.bottom = g_rectTabPages.bottom - SYMBOLTREE_MARGIN_BOTTOM ;
	}
	else
	{
		pnodeTabPage->rectScintilla.left = g_rectTabPages.left + SCINTILLA_MARGIN_LEFT ;
		pnodeTabPage->rectScintilla.right = g_rectTabPages.right - SCINTILLA_MARGIN_RIGHT  ;
		pnodeTabPage->rectScintilla.top = g_rectTabPages.top + g_nTabsHeight + SCINTILLA_MARGIN_TOP ;
		pnodeTabPage->rectScintilla.bottom = g_rectTabPages.bottom - SCINTILLA_MARGIN_BOTTOM ;

		pnodeTabPage->rectSymbolList.left = g_rectTabPages.right ;
		pnodeTabPage->rectSymbolList.right = g_rectTabPages.right ;
		pnodeTabPage->rectSymbolList.top = g_rectTabPages.top + g_nTabsHeight ;
		pnodeTabPage->rectSymbolList.bottom = g_rectTabPages.bottom ;

		pnodeTabPage->rectSymbolTree.left = g_rectTabPages.right ;
		pnodeTabPage->rectSymbolTree.right = g_rectTabPages.right ;
		pnodeTabPage->rectSymbolTree.top = g_rectTabPages.top + g_nTabsHeight ;
		pnodeTabPage->rectSymbolTree.bottom = g_rectTabPages.bottom ;
	}

	if( pnodeTabPage->hwndQueryResultEdit || pnodeTabPage->hwndQueryResultTable )
	{
		pnodeTabPage->rectScintilla.bottom -= SPLIT_WIDTH + g_stEditUltraMainConfig.nSqlQueryResultEditHeight + SPLIT_WIDTH + g_stEditUltraMainConfig.nSqlQueryResultListViewHeight ;
		pnodeTabPage->rectSymbolList.bottom -= SPLIT_WIDTH + g_stEditUltraMainConfig.nSqlQueryResultEditHeight + SPLIT_WIDTH + g_stEditUltraMainConfig.nSqlQueryResultListViewHeight ;
		pnodeTabPage->rectSymbolTree.bottom -= SPLIT_WIDTH + g_stEditUltraMainConfig.nSqlQueryResultEditHeight + SPLIT_WIDTH + g_stEditUltraMainConfig.nSqlQueryResultListViewHeight ;

		pnodeTabPage->rectQueryResultEdit.left = g_rectTabPages.left + SCINTILLA_MARGIN_LEFT ;
		pnodeTabPage->rectQueryResultEdit.right = g_rectTabPages.right - SCINTILLA_MARGIN_RIGHT ;
		pnodeTabPage->rectQueryResultEdit.top = pnodeTabPage->rectScintilla.bottom + SPLIT_WIDTH ;
		pnodeTabPage->rectQueryResultEdit.bottom = pnodeTabPage->rectQueryResultEdit.top + g_stEditUltraMainConfig.nSqlQueryResultEditHeight ;

		pnodeTabPage->rectQueryResultListView.left = g_rectTabPages.left + SCINTILLA_MARGIN_LEFT ;
		pnodeTabPage->rectQueryResultListView.right = g_rectTabPages.right - SCINTILLA_MARGIN_RIGHT ;
		pnodeTabPage->rectQueryResultListView.top = pnodeTabPage->rectQueryResultEdit.bottom + SPLIT_WIDTH ;
		pnodeTabPage->rectQueryResultListView.bottom = pnodeTabPage->rectQueryResultListView.top + g_stEditUltraMainConfig.nSqlQueryResultListViewHeight ;
	}
}

struct TabPage *AddTabPage( struct RemoteFileServer *pstRemoteFileServer , char *pcPathFilename , char *pcFilename , char *pcExtname )
{
	int		nTabPagesCount , nTabPageIndex ;
	struct TabPage	*pnodeTabPage = NULL ;
	TCITEM		tci ;
	int		nret = 0 ;

	if( pcPathFilename[0] )
	{
		nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
		for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
		{
			memset( & tci , 0x00 , sizeof(TCITEM) );
			tci.mask = TCIF_PARAM ;
			TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
			pnodeTabPage = (struct TabPage *)(tci.lParam);
			if( strcmp( pnodeTabPage->acPathFilename , pcPathFilename ) == 0 )
			{
				SelectTabPage( pnodeTabPage , nTabPageIndex );
				return NULL;
			}
		}
	}

	pnodeTabPage = (struct TabPage *)malloc( sizeof(struct TabPage) ) ;
	if (pnodeTabPage == NULL)
	{
		::MessageBox(NULL, TEXT("申请内存用于TABPAGE失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return NULL;
	}
	memset( pnodeTabPage , 0x00 , sizeof(struct TabPage) );

	if( pstRemoteFileServer )
		memcpy( & (pnodeTabPage->stRemoteFileServer) , pstRemoteFileServer , sizeof(struct RemoteFileServer) );
	strcpy( pnodeTabPage->acPathFilename , pcPathFilename );
	strcpy( pnodeTabPage->acFilename , pcFilename );
	pnodeTabPage->nFilenameLen = strlen(pnodeTabPage->acFilename) ;
	strcpy( pnodeTabPage->acPathName , pcPathFilename ); pnodeTabPage->acPathName[strlen(pnodeTabPage->acPathName)-pnodeTabPage->nFilenameLen] = '\0' ;

	pnodeTabPage->pstDocTypeConfig = GetDocTypeConfig( pcExtname ) ;

	nTabPagesCount = TabCtrl_GetItemCount(g_hwndTabPages) ;

	memset( & tci , 0x00 , sizeof(TCITEM) );
	tci.mask = TCIF_TEXT|TCIF_PARAM ;
	tci.pszText = pnodeTabPage->acFilename ;
	tci.lParam = (LPARAM)pnodeTabPage ;
	nret = TabCtrl_InsertItem( g_hwndTabPages , nTabPagesCount , & tci ) ;
	if( nret == -1 )
	{
		::MessageBox(NULL, TEXT("TabCtrl_InsertItem失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return NULL;
	}

	nret = CreateScintillaControl( pnodeTabPage ) ;
	if (nret)
	{
		::MessageBox(NULL, TEXT("CreateScintillaControl失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return NULL;
	}

	// SelectTabPageByIndex( nTabPagesCount );
	
	return pnodeTabPage;
}

int RemoveTabPage( struct TabPage *pnodeTabPage )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	struct TabPage	*p = NULL ;
	TCITEM		tci ;
	int		nret = 0 ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		if( p == pnodeTabPage )
		{
			if( nTabPagesCount == 1 )
			{
				g_pnodeCurrentTabPage = NULL ;
				SetWindowTitle( NULL );
			}
			else if( nTabPageIndex == 0 )
				SelectTabPageByIndex( nTabPageIndex+1 );
			else if( nTabPageIndex == nTabPagesCount - 1 )
				SelectTabPageByIndex( nTabPageIndex-1 );
			else if( nTabPageIndex < nTabPagesCount )
				SelectTabPageByIndex( nTabPageIndex+1 );

			DestroyScintillaControl( pnodeTabPage );

			::DestroyWindow( pnodeTabPage->hwndSymbolList );

			TabCtrl_DeleteItem( g_hwndTabPages , nTabPageIndex );

			break;
		}
	}

	return 0;
}

void SetTabPageTitle( int nTabPageNo , char *title )
{
	TCITEM		tci ;

	memset( & tci , 0x00 , sizeof(TCITEM) );
	tci.mask = TCIF_TEXT ;
	tci.pszText = title ;
	TabCtrl_SetItem( g_hwndTabPages , nTabPageNo , & tci );
	::InvalidateRect( g_hwndTabPages , NULL , true );
	
	return;
}

void SelectTabPage( struct TabPage *pnodeTabPage , int nTabPageIndex )
{
	int		nTabPagesCount ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	int		nret = 0 ;

	if( nTabPageIndex < 0 )
	{
		nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
		for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
		{
			memset( & tci , 0x00 , sizeof(TCITEM) );
			tci.mask = TCIF_PARAM ;
			TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
			p = (struct TabPage *)(tci.lParam);
			if( p == pnodeTabPage )
				break;
		}
		if( nTabPageIndex >= nTabPagesCount )
			return;
	}

	TabCtrl_SetCurSel( g_hwndTabPages , nTabPageIndex );
	g_pnodeCurrentTabPage = pnodeTabPage ;

	SetWindowTitle( g_pnodeCurrentTabPage->acPathFilename );

	SetCurrentTabPageHighLight( nTabPageIndex );

	if( pnodeTabPage->stRemoteFileServer.acNetworkAddress[0] == '\0' && pnodeTabPage->acPathFilename[0] && g_stEditUltraMainConfig.bCheckUpdateWhereSelectTabPage == TRUE && pnodeTabPage->bNotPromptWhereAutoUpdate == FALSE )
	{
		struct stat	statbuf ;

		nret = stat( pnodeTabPage->acPathFilename , & statbuf ) ;
		if( nret == -1 )
		{
			char	msg[100 + MAX_PATH];
			int	decision;
			_snprintf(msg, sizeof(msg)-1, "侦测到文件[%s]被改名或删除，是否保留 ?" , pnodeTabPage->acPathFilename);
			p = g_pnodeCurrentTabPage ; g_pnodeCurrentTabPage = NULL ;
			decision = ::MessageBox(NULL, msg, g_acAppName, MB_YESNO);
			g_pnodeCurrentTabPage = p ;
			if (decision == IDNO)
			{
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETSAVEPOINT, 0, 0) ;
				OnCloseFile( pnodeTabPage );
				pnodeTabPage = g_pnodeCurrentTabPage ;
			}
			else if (decision == IDYES)
			{
				pnodeTabPage->acPathFilename[0] = '\0' ;

				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_INSERTTEXT, 0, (sptr_t)"X" );
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_DELETERANGE, 0, 1 );
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_ENDUNDOACTION, 0, 0 );
			}
		}
		else if( statbuf.st_mtime > pnodeTabPage->st_mtime )
		{
			if( IsDocumentModified(pnodeTabPage) == TRUE )
			{
				if( g_stEditUltraMainConfig.bPromptWhereAutoUpdate == TRUE )
				{
					p = g_pnodeCurrentTabPage ; g_pnodeCurrentTabPage = NULL ;
					int decision = ::MessageBox(NULL, "外部文件已发生变化，是否丢弃本次修改重新打开(Yes)，或始终无视外部变化(No)", "询问", MB_ICONQUESTION|MB_YESNO|MB_TASKMODAL);
					g_pnodeCurrentTabPage = p ;
					if( decision == IDYES )
					{
						goto _GOTO_RELOAD_FILE_FORCELY;
					}
					else if( decision == IDNO )
					{
						pnodeTabPage->bNotPromptWhereAutoUpdate = TRUE ;
					}
				}
				else
				{
					goto _GOTO_RELOAD_FILE_FORCELY;
				}
			}
			else if( IsDocumentModified(pnodeTabPage) == FALSE )
			{
				if( g_stEditUltraMainConfig.bPromptWhereAutoUpdate == TRUE )
				{
					p = g_pnodeCurrentTabPage ; g_pnodeCurrentTabPage = NULL ;
					::MessageBox(NULL, "外部文件已发生变化，重新加载文件", "信息", MB_OK|MB_TASKMODAL);
					g_pnodeCurrentTabPage = p ;
				}

_GOTO_RELOAD_FILE_FORCELY :
				int		nCurrentPos ;
				int		nCurrentLine ;
				int		nMaxLine ;

				nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
				nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;

				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_CLEARALL, 0, 0 );
				nret = LoadFileDirectly( pnodeTabPage->acPathFilename , NULL , pnodeTabPage ) ;
				if( nret )
					return;

				nMaxLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETLINECOUNT, 0, 0 ) ;
				if( nCurrentLine > nMaxLine-1 )
					nCurrentLine = nMaxLine-1 ;

				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETUNDOCOLLECTION, 1, 0);
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETSAVEPOINT, 0, 0);

				JumpGotoLine( pnodeTabPage , nCurrentLine , 0 );

				pnodeTabPage->st_mtime = statbuf.st_mtime ;
			}
		}
	}

	// 重新计算各窗口大小
	UpdateAllWindows( g_hwndMainWindow );

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	return;
}

struct TabPage *GetTabPageByScintilla( void *hwndScintilla )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		p = GetTabPageByIndex( nTabPageIndex ) ;
		if( p->hwndScintilla == hwndScintilla )
			return p;
	}

	return NULL;
}

struct TabPage *GetTabPageByIndex( int nTabPageIndex )
{
	TCITEM		tci ;

	int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	if( nTabPageIndex < 0 || nTabPageIndex >= nTabPagesCount )
		return NULL;

	memset( & tci , 0x00 , sizeof(TCITEM) );
	tci.mask = TCIF_PARAM ;
	TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
	return (struct TabPage *)(tci.lParam);
}

struct TabPage *SelectTabPageByIndex( int nTabPageIndex )
{
	struct TabPage	*p = NULL ;

	p = GetTabPageByIndex( nTabPageIndex ) ;
	if( p )
	{
		SelectTabPage( p , nTabPageIndex );
		return p;
	}

	return NULL;
}

void SetCurrentTabPageHighLight( int nCurrentTabPageIndex )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	
	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		if( nTabPageIndex == nCurrentTabPageIndex )
		{
			TabCtrl_HighlightItem(g_hwndTabPages,nTabPageIndex,1.00);
		}
		else
		{
			TabCtrl_HighlightItem(g_hwndTabPages,nTabPageIndex,0.00);
		}
	}

	return;
}

int OnSelectChange()
{
	int nPageNo = TabCtrl_GetCurSel(g_hwndTabPages) ;
	SelectTabPageByIndex( nPageNo );

	return 0;
}

int OnSymbolListDbClick( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage && pnodeTabPage->pstDocTypeConfig && pnodeTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolList )
	{
		return pnodeTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolList( pnodeTabPage );
	}
	else
	{
		return -1;
	}
}

int CalcTabPagesHeight()
{
	int	top = -1 , bottom = -1 ;

	int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	RECT rectTabPage ;
	for( int nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
		DEBUGLOGC("index[%d] top[%d] bottom[%d]",nTabPageIndex,rectTabPage.top,rectTabPage.bottom)
		if( top == -1 || rectTabPage.top < top )
		{
			top = rectTabPage.top ;
		}
		if( bottom == -1 || rectTabPage.bottom > bottom )
		{
			bottom = rectTabPage.bottom ;
		}
	}

	if( top == -1 || bottom == -1 )
		g_nTabsHeight = TABS_HEIGHT_DEFAULT ;
	else
		g_nTabsHeight = bottom - top + 1 ;

	return 0;
}
