#ifndef _H_EDITULTRA_REMOTE_FILESERVER_
#define _H_EDITULTRA_REMOTE_FILESERVER_

#include "framework.h"

#define DIRBUF_MAX	1024*1024

struct RemoteFileServer
{
	char			acFileServerName[ 100 ] ;
	char			acCommProtocol[ 20 ] ;
	char			acNetworkAddress[ 40 ] ;
	int			nNetworkPort ;
	int			nAccessArea ;
	char			acLoginUser[ 20 ] ;
	char			acLoginPass[ 32+1 ] ; BOOL bConfigLoginPass ;

	struct list_head	nodeRemoteFileServer ;
};

INT_PTR CALLBACK RemoteFileServersWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

int LoadRemoteFileServersConfigFromFile( char *filebuf );
int SaveRemoteFileServersConfigToFile();

int OnManageRemoteFileServers();

#endif
