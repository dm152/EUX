#include "framework.h"

int GetFileNameByOpenFileDialog( char* pcFilename, int nFilenameBufSize)
{
	OPENFILENAME ofn = { sizeof(OPENFILENAME) };
	ofn.hwndOwner = g_hwndMainWindow;
	ofn.hInstance = g_hAppInstance;
	memset(pcFilename, 0x00, nFilenameBufSize);
	ofn.lpstrFile = pcFilename;
	ofn.nMaxFile = nFilenameBufSize;
	ofn.lpstrFilter = (LPCSTR)GetFileDialogFilterPtr() ;
	ofn.lpstrTitle = "打开文件";
	ofn.Flags = OFN_ENABLESIZING | OFN_EXPLORER | OFN_ALLOWMULTISELECT | OFN_HIDEREADONLY ;
	if( ::GetOpenFileName( & ofn ) == FALSE )
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int GetFileNameBySaveFileDialog( char *pcFilename, int nFilenameBufSize)
{
	OPENFILENAME ofn = { sizeof(ofn) };
	ofn.hwndOwner = g_hwndMainWindow;
	ofn.hInstance = g_hAppInstance;
	memset(pcFilename, 0x00, nFilenameBufSize);
	ofn.lpstrFile = pcFilename;
	ofn.nMaxFile = nFilenameBufSize;
	ofn.lpstrFilter = GetFileDialogFilterPtr() ;
	/*
	ofn.nFilterIndex = 0 ;
	*/
	ofn.lpstrTitle = "保存文件";
	ofn.Flags = OFN_ENABLESIZING | OFN_HIDEREADONLY;
	if( ::GetSaveFileName( & ofn ) == FALSE )
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int SplitPathFilename( char *acFullPathFilename , char *acDrivename , char *acPathname , char *acFilename , char *acMainFilename , char *acExtFilename , char *acDriveAndPathname , char *acPathFilename )
{
	char	*p1 = NULL ;
	char	*p2 = NULL ;
	char	*pcPathnameBase = NULL ;
	char	*pcFilenameBase = NULL ;
	
	p1 = strchr( acFullPathFilename , ':' ) ;
	if( p1 )
	{
		pcPathnameBase = p1 + 1 ;
		if( acDrivename )
			sprintf( acDrivename , "%.*s" , (int)(pcPathnameBase-acFullPathFilename) , acFullPathFilename );
	}
	else
	{
		pcPathnameBase = acFullPathFilename ;
		if( acDrivename )
			acDrivename[0] = '\0' ;
	}

	if( acDrivename )
		sprintf( acDrivename , "%.*s" , (int)(pcPathnameBase-acFullPathFilename) , acFullPathFilename );

	p1 = strrchr( pcPathnameBase , '/' ) ;
	p2 = strrchr( pcPathnameBase , '\\' ) ;
	if( p1 == NULL && p2 == NULL )
	{
		p1 = pcPathnameBase ;
	}
	else if( p1 && p2 )
	{
		if( p1 < p2 )
			p1 = p2 + 1 ;
		else
			p1++;
	}
	else
	{
		if( p2 )
			p1 = p2 + 1 ;
		else
			p1++;
	}

	pcFilenameBase = p1 ;
	if( acPathname )
		sprintf( acPathname , "%.*s" , (int)(pcFilenameBase-pcPathnameBase) , pcPathnameBase );

	if( acFilename )
		strcpy( acFilename , pcFilenameBase );

	p1 = strrchr( pcFilenameBase , '.' ) ;
	if( p1 )
	{
		if( acMainFilename )
			sprintf( acMainFilename , "%.*s" , (int)(p1-pcFilenameBase) , pcFilenameBase );
		if( acExtFilename )
			strcpy( acExtFilename , p1 );
	}
	else
	{
		if( acMainFilename )
			strcpy( acMainFilename , pcFilenameBase );
		if( acExtFilename )
			acExtFilename[0] = '\0' ;
	}

	if( acDriveAndPathname )
		sprintf( acDriveAndPathname , "%.*s" , (int)(pcFilenameBase-acFullPathFilename) , acFullPathFilename );

	if( acPathFilename )
		strcpy( acPathFilename , pcPathnameBase );

	return 0;
}

int test_SplitPathFilename()
{
	int		nret = 0 ;

	char acFullPathFilename[] = "C:\\path1\\file1.ext1" ;
	char acDrivename[MAX_PATH] = "" ;
	char acPathname[MAX_PATH] = "" ;
	char acFilename[MAX_PATH] = "" ;
	char acMainFilename[MAX_PATH] = "" ;
	char acExtFilename[MAX_PATH] = "" ;
	char acDriveAndPathname[MAX_PATH] = "" ;
	char acPathFilename[MAX_PATH] = "" ;

	nret = SplitPathFilename( acFullPathFilename , acDrivename , acPathname , acFilename , acMainFilename , acExtFilename , acDriveAndPathname , acPathFilename ) ;

	return nret;
}

int OnNewFile( struct TabPage *pnodeTabPage )
{
	int		nret = 0;

	pnodeTabPage = AddTabPage( NULL , (char*)"" , (char*)"未保存的新建文档" , (char*)"" ) ;
	if( pnodeTabPage == NULL )
		return -1;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETEOLMODE , g_stEditUltraMainConfig.nNewFileEols , 0 ) ;

	InitTabPageControlsBeforeLoadFile( pnodeTabPage );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CLEARALL , 0 , 0 );

	pnodeTabPage->nCodePage = GetACP() ;

	SetWindowTitle(pnodeTabPage->acPathFilename);

	int nTabPagesCount = TabCtrl_GetItemCount(g_hwndTabPages) ;
	SelectTabPageByIndex( nTabPagesCount-1 );

	InitTabPageControlsAfterLoadFile( pnodeTabPage );
	AddNavigateBackNextTrace( pnodeTabPage , 0 );

	g_pnodeCurrentTabPage = pnodeTabPage ;

	// 重新计算各窗口大小
	CalcTabPagesHeight();
	UpdateAllWindows( g_hwndMainWindow );

	return 0;
}

int LoadFileDirectly( char *acFullPathFilename , FILE *fp , struct TabPage *pnodeTabPage )
{
	size_t			sBlockNo ;
	int			nNewLineMode ;
	size_t			offset ;

	if( fp == NULL )
	{
		fp = fopen( acFullPathFilename , "rb" );
		if( fp == NULL )
		{
			ErrorBox( "不能打开文件[%s]" , acFullPathFilename );
			return EUX_ERROR_CANT_OPEN_FILE;
		}
	}

	InitEncodingProbe( & (pnodeTabPage->stEncodingProbe) );
	InitNewLineModeProbe( & (pnodeTabPage->stNewLineModeProbe) );

	pnodeTabPage->nCodePage = 0 ;

	char data[ 64 * 1024 ];
	size_t len;
	for( sBlockNo = 0 ; ; )
	{
		len = fread( data , 1 , sizeof(data) , fp );
		if (len < 0)
			return -4;
		if (len == 0)
			break;

		sBlockNo++;

		if(
			sBlockNo == 1
			&&
			memcmp( data , "\xEF\xBB\xBF" , 3 ) == 0
		)
		{
			pnodeTabPage->nCodePage = 65001 ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCODEPAGE , pnodeTabPage->nCodePage , 0 );
			pnodeTabPage->nPreFileContextLen = 3 ;
			memcpy( pnodeTabPage->acPreFileContext , data , pnodeTabPage->nPreFileContextLen );
			offset = pnodeTabPage->nPreFileContextLen ;
		}
		else if(
			sBlockNo == 1
			&&
			(
				memcmp( data , "\x00\x00\xFE\xFF" , 4 ) == 0
				||
				memcmp( data , "\xFF\xFE\x00\x00" , 4 ) == 0
			)
		)
		{
			pnodeTabPage->nCodePage = 65001 ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCODEPAGE , pnodeTabPage->nCodePage , 0 );
			pnodeTabPage->nPreFileContextLen = 4 ;
			memcpy( pnodeTabPage->acPreFileContext , data , pnodeTabPage->nPreFileContextLen );
			offset = pnodeTabPage->nPreFileContextLen ;
		}
		else if( sBlockNo == 1 && memcmp( data , "\xFF\xFE" , 2 ) == 0 )
		{
			pnodeTabPage->nCodePage = 1200 ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCODEPAGE , pnodeTabPage->nCodePage , 0 );
			pnodeTabPage->nPreFileContextLen = 2 ;
			memcpy( pnodeTabPage->acPreFileContext , data , pnodeTabPage->nPreFileContextLen );
			offset = pnodeTabPage->nPreFileContextLen ;
		}
		else if( sBlockNo == 1 && memcmp( data , "\xFE\xFF" , 2 ) == 0 )
		{
			pnodeTabPage->nCodePage = 1201 ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCODEPAGE , pnodeTabPage->nCodePage , 0 );
			pnodeTabPage->nPreFileContextLen = 2 ;
			memcpy( pnodeTabPage->acPreFileContext , data , pnodeTabPage->nPreFileContextLen );
			offset = pnodeTabPage->nPreFileContextLen ;
		}
		else
		{
			DoEncodingProbe( & (pnodeTabPage->stEncodingProbe) , data , len );
			offset = 0 ;
		}

		DoNewLineModeProbe( & (pnodeTabPage->stNewLineModeProbe) , data , len );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_ADDTEXT, len-offset, reinterpret_cast<LPARAM>(data+offset));
	}

	fclose(fp);

	struct stat	statbuf ;
	stat( pnodeTabPage->acPathFilename , & statbuf );
	pnodeTabPage->st_mtime = statbuf.st_mtime ;

	if( pnodeTabPage->nCodePage == 0 )
	{
		pnodeTabPage->nCodePage = GetCodePageFromEncodingProbe( & (pnodeTabPage->stEncodingProbe) ) ;
		if( pnodeTabPage->nCodePage == 0 )
			pnodeTabPage->nCodePage = GetACP() ;
	}

	nNewLineMode = GetNewLineModeFromProbe( & (pnodeTabPage->stNewLineModeProbe) , g_stEditUltraMainConfig.nNewFileEols ) ;
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETEOLMODE, nNewLineMode, 0 );

	return 0;
}

int OpenFileDirectly( char *acFullPathFilename )
{
	FILE			*fp = NULL ;
	char			acFilename[ MAX_PATH ] ;
	char			acExtname[ MAX_PATH ] ;
	struct TabPage		*pnodeTabPage = NULL ;
	int			nret = 0 ;

	if( strncmp(acFullPathFilename,"\\\\",2) && acFullPathFilename[1]!=':' )
	{
		char tmp[MAX_PATH];
		memset( tmp , 0x00 , sizeof(tmp) );
		::GetCurrentDirectory( sizeof(tmp)-1 , tmp );
		snprintf( tmp + strlen(tmp) , sizeof(tmp)-1 - strlen(tmp) , "\\%s" , acFullPathFilename );
		strcpy( acFullPathFilename , tmp );
	}

	SplitPathFilename( acFullPathFilename , NULL , NULL , acFilename , NULL , acExtname , NULL , NULL );

	fp = fopen( acFullPathFilename , "rb" );
	if (fp == NULL)
	{
		ErrorBox( "不能打开文件[%s]" , acFullPathFilename );
		return -2;
	}

	pnodeTabPage = AddTabPage( NULL , acFullPathFilename , acFilename , acExtname ) ;
	if( pnodeTabPage == NULL )
	{
		fclose(fp);
		return -3;
	}

	InitTabPageControlsBeforeLoadFile( pnodeTabPage );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CLEARALL , 0 , 0 );

	nret = LoadFileDirectly( acFullPathFilename , fp , pnodeTabPage ) ;
	if( nret )
		return nret;
	
	/*
	strcpy( pnodeTabPage->acPathFilename, acFullPathFilename );
	strcpy( pnodeTabPage->acFilename, acFilename );
	pnodeTabPage->nFilenameLen = strlen(pnodeTabPage->acFilename) ;
	strcpy( pnodeTabPage->acPathName , acFullPathFilename ); pnodeTabPage->acPathName[strlen(pnodeTabPage->acPathName)-pnodeTabPage->nFilenameLen] = '\0' ;
	*/
	
	SetWindowTitle(pnodeTabPage->acPathFilename);

	int nTabPagesCount = TabCtrl_GetItemCount(g_hwndTabPages) ;
	SelectTabPageByIndex( nTabPagesCount-1 );

	InitTabPageControlsAfterLoadFile( pnodeTabPage );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOPOS, 0, 0);
	AddNavigateBackNextTrace( pnodeTabPage , 0 );
	
	/* 如果设置了打开文件后锁定文件 */
	if( g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile == TRUE )
	{
		DWORD dwFileAttr = ::GetFileAttributes( pnodeTabPage->acPathFilename ) ;
		dwFileAttr |= FILE_ATTRIBUTE_READONLY ;
		::SetFileAttributes( pnodeTabPage->acPathFilename , dwFileAttr );
	}

	PushOpenPathFilenameRecently( pnodeTabPage->acPathFilename );

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	// 重新计算各窗口大小
	CalcTabPagesHeight();
	UpdateAllWindows( g_hwndMainWindow );

	// 设置焦点到编辑控件
	::SetFocus( pnodeTabPage->hwndScintilla );

	return 0;
}

int OpenFilesDirectly( char *acFileNames )
{
	char			acPathname[ MAX_PATH ] ;
	char			acMainFilename[ MAX_PATH ] ;
	char			acExtFilename[ MAX_PATH ] ;
	char			acDriveAndPathname[ MAX_PATH ] ;
	WIN32_FIND_DATA		stFindFileData ;
	HANDLE			hFindFile ;
	char			acFoundFilename[ MAX_PATH ] ;
	int			nret = 0 ;

	nret = SplitPathFilename( acFileNames , NULL , acPathname , NULL , acMainFilename , acExtFilename , acDriveAndPathname , NULL ) ;
	if( nret )
	{
		ErrorBox( "分解路径文件名[%s]失败" , acFileNames );
		return nret;
	}

	hFindFile = ::FindFirstFile( acFileNames , & stFindFileData ) ;
	if( hFindFile == INVALID_HANDLE_VALUE )
	{
		ErrorBox( "文件[%s]不存在" , acFileNames );
		return 1;
	}

	do
	{
		if( strcmp(stFindFileData.cFileName,".") == 0 || strcmp(stFindFileData.cFileName,"..") == 0 )
			continue;

		if( ! ( stFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
		{
			memset( acFoundFilename , 0x00 , sizeof(acFoundFilename) );
			if( strchr( acMainFilename,'*') || strchr(acExtFilename,'*') )
				_snprintf( acFoundFilename , sizeof(acFoundFilename)-1 , "%s%s" , acDriveAndPathname , stFindFileData.cFileName ) ;
			else
				strcpy( acFoundFilename , acFileNames );
			OpenFileDirectly( acFoundFilename );
		}
	}
	while( ::FindNextFile( hFindFile , & stFindFileData ) );

	::FindClose( hFindFile );

	return 0;
}

#define PATHFILENAMES_BUFSIZE	30*1024

int OnOpenFile()
{
	char		*acPathFilenames = NULL ;
	char		acPathname[ MAX_PATH ] ;
	int		nPathnameLen ;
	char		*p = NULL ;
	char		acPathFilename[ MAX_PATH ] ;

	int		nret = 0;

	acPathFilenames = (char*)malloc( PATHFILENAMES_BUFSIZE ) ;
	if( acPathFilenames == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存以存放打开文件名集缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acPathFilenames , 0x00 , PATHFILENAMES_BUFSIZE );

	nret = GetFileNameByOpenFileDialog( acPathFilenames , PATHFILENAMES_BUFSIZE ) ;
	if (nret)
		return -1;

	DWORD dwFileAttributes = ::GetFileAttributes( acPathFilenames ) ;
	if( ! ( dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
	{
		nret = OpenFileDirectly( acPathFilenames ) ;
		if( nret )
		{
			return -1;
		}
	}
	else
	{
		memset( acPathname , 0x00 , sizeof(acPathname) );
		strncpy( acPathname , acPathFilenames , sizeof(acPathname)-1 );
		nPathnameLen = (int)strlen( acPathname ) ;
	
		p = acPathFilenames + nPathnameLen + 1 ;
		while( *p )
		{
			memset( acPathFilename , 0x00 , sizeof(acPathFilename) );
			snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s\\%s" , acPathname , p );
			nret = OpenFileDirectly( acPathFilename ) ;
			if( nret )
			{
				return -1;
			}

			p = p + strlen(p) + 1 ;
		}
	}

	return 0;
}

int OnCleanOpenRecentlyHistory( struct TabPage *pnodeTabPage )
{
	memset( g_stEditUltraMainConfig.aacOpenPathFilenameRecently , 0x00 , sizeof(g_stEditUltraMainConfig.aacOpenPathFilenameRecently) );

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	return 0;
}

int OnOpenDropFile( HDROP hDrop )
{
	int		nFileCount ;
	int		nFileIndex ;
	char		acPathFilename[ MAX_PATH ] ;

	int		nret = 0 ;

	nFileCount = DragQueryFile( hDrop , 0xFFFFFFFF , NULL , 0 );
	for( nFileIndex = 0 ; nFileIndex < nFileCount ; nFileIndex++ )
	{
		memset( acPathFilename , 0x00 , sizeof(acPathFilename) );
		DragQueryFile( hDrop , nFileIndex , acPathFilename , sizeof(acPathFilename) );
		if( GetFileType(acPathFilename) == FILETYPE_DIRECTORY )
			strcat(acPathFilename,"\\*"),OpenFilesDirectly( acPathFilename );
		else
			OpenFileDirectly( acPathFilename );
	}

	DragFinish( hDrop );

	return 0;
}

static size_t ReadRemoteFile(void *buffer, size_t size, size_t nmemb, void *stream)
{
	struct TabPage		*pnodeTabPage = (struct TabPage *)stream ;
	char			*data = (char*)buffer ;
	size_t			len ;
	size_t			offset ;

	len = size * nmemb ;

	if( pnodeTabPage->nFileSize == 0 )
	{
		if(
			memcmp( data , "\xEF\xBB\xBF" , 3 ) == 0
		)
		{
			pnodeTabPage->nCodePage = 65001 ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCODEPAGE , pnodeTabPage->nCodePage , 0 );
			pnodeTabPage->nPreFileContextLen = 3 ;
			memcpy( pnodeTabPage->acPreFileContext , data , pnodeTabPage->nPreFileContextLen );
			offset = pnodeTabPage->nPreFileContextLen ;
		}
		else if(
			memcmp( data , "\x00\x00\xFE\xFF" , 4 ) == 0
			||
			memcmp( data , "\xFF\xFE\x00\x00" , 4 ) == 0
		)
		{
			pnodeTabPage->nCodePage = 65001 ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCODEPAGE , pnodeTabPage->nCodePage , 0 );
			pnodeTabPage->nPreFileContextLen = 4 ;
			memcpy( pnodeTabPage->acPreFileContext , data , pnodeTabPage->nPreFileContextLen );
			offset = pnodeTabPage->nPreFileContextLen ;
		}
		else if(
			memcmp( data , "\xFF\xFE" , 2 ) == 0
			)
		{
			pnodeTabPage->nCodePage = 1200 ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCODEPAGE , pnodeTabPage->nCodePage , 0 );
			pnodeTabPage->nPreFileContextLen = 2 ;
			memcpy( pnodeTabPage->acPreFileContext , data , pnodeTabPage->nPreFileContextLen );
			offset = pnodeTabPage->nPreFileContextLen ;
		}
		else if(
			memcmp( data , "\xFE\xFF" , 2 ) == 0
			)
		{
			pnodeTabPage->nCodePage = 1201 ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETCODEPAGE , pnodeTabPage->nCodePage , 0 );
			pnodeTabPage->nPreFileContextLen = 2 ;
			memcpy( pnodeTabPage->acPreFileContext , data , pnodeTabPage->nPreFileContextLen );
			offset = pnodeTabPage->nPreFileContextLen ;
		}
		else
		{
			DoEncodingProbe( & (pnodeTabPage->stEncodingProbe) , data , len );
			offset = 0 ;
		}
	}
	else
	{
		DoEncodingProbe( & (pnodeTabPage->stEncodingProbe) , data , len );
		offset = 0 ;
	}

	DoNewLineModeProbe( & (pnodeTabPage->stNewLineModeProbe) , data , len );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_ADDTEXT, len-offset, reinterpret_cast<LPARAM>(data+offset));
	pnodeTabPage->nFileSize += len-offset ;

	return len;
}

int OpenRemoteFileDirectly( struct RemoteFileServer *pstRemoteFileServer , char *acFullPathFilename )
{
	CURL			*curl = NULL ;
	CURLcode		res ;
	char			url[ 1024 ] ;
	char			userpwd[ 256 ] ;
	char			acFilename[ MAX_PATH ] ;
	char			acExtname[ MAX_PATH ] ;
	struct TabPage		*pnodeTabPage = NULL ;
	int			nNewLineMode ;

	SplitPathFilename( acFullPathFilename , NULL , NULL , acFilename , NULL , acExtname , NULL , NULL );

	curl = curl_easy_init() ;
	if( curl == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -2;
	}

	pnodeTabPage = AddTabPage( pstRemoteFileServer , acFullPathFilename , acFilename , acExtname ) ;
	if( pnodeTabPage == NULL )
	{
		return -3;
	}

	InitTabPageControlsBeforeLoadFile( pnodeTabPage );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_CLEARALL , 0 , 0 );

	InitEncodingProbe( & (pnodeTabPage->stEncodingProbe) );
	InitNewLineModeProbe( & (pnodeTabPage->stNewLineModeProbe) );

	pnodeTabPage->nCodePage = 0 ;

	memset( url , 0x00 , sizeof(url) );
	strncpy( url , acFullPathFilename , sizeof(url)-1 );
	curl_easy_setopt( curl , CURLOPT_URL , url );
	memset( userpwd , 0x00 , sizeof(userpwd) );
	snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , pstRemoteFileServer->acLoginUser , pstRemoteFileServer->acLoginPass );
	curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
	curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
	curl_easy_setopt( curl , CURLOPT_WRITEFUNCTION , ReadRemoteFile );
	curl_easy_setopt( curl , CURLOPT_WRITEDATA , pnodeTabPage );
	curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
	curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
	res = curl_easy_perform( curl ) ;
	if( res != CURLE_OK )
	{
		::MessageBox(NULL, TEXT("连接远程文件服务器失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		curl_easy_cleanup( curl );
		return -3;
	}
	else
	{
		curl_easy_cleanup( curl );
	}

	/*
	memcpy( & (pnodeTabPage->stRemoteFileServer) , pstRemoteFileServer , sizeof(struct RemoteFileServer) );
	strcpy( pnodeTabPage->acPathFilename, acFullPathFilename );
	strcpy( pnodeTabPage->acFilename, acFilename );
	pnodeTabPage->nFilenameLen = strlen(pnodeTabPage->acFilename) ;
	*/

	if( pnodeTabPage->nCodePage == 0 )
	{
		pnodeTabPage->nCodePage = GetCodePageFromEncodingProbe( & (pnodeTabPage->stEncodingProbe) ) ;
		if( pnodeTabPage->nCodePage == 0 )
			pnodeTabPage->nCodePage = GetACP() ;
	}

	nNewLineMode = GetNewLineModeFromProbe( & (pnodeTabPage->stNewLineModeProbe) , g_stEditUltraMainConfig.nNewFileEols ) ;
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETEOLMODE, nNewLineMode, 0 );

	SetWindowTitle(pnodeTabPage->acPathFilename);

	int nTabPagesCount = TabCtrl_GetItemCount(g_hwndTabPages) ;
	SelectTabPageByIndex( nTabPagesCount-1 );

	InitTabPageControlsAfterLoadFile( pnodeTabPage );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOPOS, 0, 0);
	AddNavigateBackNextTrace( pnodeTabPage , 0 );

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	// 重新计算各窗口大小
	CalcTabPagesHeight();
	UpdateAllWindows( g_hwndMainWindow );

	// 设置焦点到编辑控件
	::SetFocus( pnodeTabPage->hwndScintilla );

	return 0;
}

static int DoWriteFile( struct TabPage *pnodeTabPage , char* pathfilename)
{
	FILE	*fp = NULL ;
	size_t	nWriteLen ;
	char	acWriteBuffer[ 64*1024 ] ;
	char	*pcWriteBuffer = NULL ;

	/* 如果设置了打开文件后锁定文件 */
	if( g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile == TRUE )
	{
		DWORD dwFileAttr = ::GetFileAttributes( pathfilename ) ;
		dwFileAttr &= ~FILE_ATTRIBUTE_READONLY ;
		::SetFileAttributes( pathfilename , dwFileAttr );
	}

	fp = fopen(pathfilename, "wb");
	if( fp == NULL )
	{
		char msg[MAX_PATH + 100];
		_snprintf( msg , sizeof(msg)-1 , "不能打开文件[%s]" , pathfilename );
		::MessageBox(g_hwndMainWindow, msg, g_acAppName, MB_OK);
		return -1;
	}

	if( pnodeTabPage->nPreFileContextLen > 0 )
	{
		size_t len = fwrite( pnodeTabPage->acPreFileContext , pnodeTabPage->nPreFileContextLen , 1 , fp );
		if( len == -1 )
		{
			::MessageBox(g_hwndMainWindow, "写文件失败", g_acAppName, MB_OK);
			fclose(fp);
			return -2;
		}
	}

	if( pnodeTabPage->bHexEditMode == FALSE )
	{
		pnodeTabPage->nFileSize = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETLENGTH, 0, 0);
	}
	else
	{
		pnodeTabPage->acWriteFileBuffer = (char*)StrdupHexEditTextFold( pnodeTabPage , & pnodeTabPage->nFileSize ) ;
		if( pnodeTabPage->acWriteFileBuffer == NULL )
			return -3;
	}

	for ( pnodeTabPage->nWroteLen = 0 ; pnodeTabPage->nWroteLen < pnodeTabPage->nFileSize ; )
	{
		nWriteLen = pnodeTabPage->nFileSize - pnodeTabPage->nWroteLen;
		if(nWriteLen > sizeof(acWriteBuffer)-1 )
			nWriteLen = sizeof(acWriteBuffer)-1 ;
		if( pnodeTabPage->bHexEditMode == FALSE )
		{
			GetTextByRange( pnodeTabPage , pnodeTabPage->nWroteLen , pnodeTabPage->nWroteLen+nWriteLen , acWriteBuffer ) ;
			pcWriteBuffer = acWriteBuffer ;
		}
		else
		{
			pcWriteBuffer = pnodeTabPage->acWriteFileBuffer + pnodeTabPage->nWroteLen ;
		}
		size_t len = fwrite( pcWriteBuffer , nWriteLen , 1 , fp );
		if( len == -1 )
		{
			::MessageBox(g_hwndMainWindow, "写文件失败", g_acAppName, MB_OK);
			fclose(fp);
			return -2;
		}

		pnodeTabPage->nWroteLen += nWriteLen ;
	}

	if( pnodeTabPage->bHexEditMode == FALSE )
	{
		;
	}
	else
	{
		free( pnodeTabPage->acWriteFileBuffer ); pnodeTabPage->acWriteFileBuffer = NULL ;
	}

	fclose(fp);

	/* 如果设置了打开文件后锁定文件 */
	if( g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile == TRUE )
	{
		DWORD dwFileAttr = ::GetFileAttributes( pathfilename ) ;
		dwFileAttr |= FILE_ATTRIBUTE_READONLY ;
		::SetFileAttributes( pathfilename , dwFileAttr );
	}

	return 0;
}

static size_t WriteRemoteFile(void *buffer, size_t size, size_t nmemb, void *stream)
{
	struct TabPage		*pnodeTabPage = (struct TabPage *)stream ;
	size_t			len ;

	if( pnodeTabPage->nWroteLen >= pnodeTabPage->nFileSize )
		return 0;

	if( pnodeTabPage->bNeedWrotePreFileContext == TRUE )
	{
		memcpy( buffer , pnodeTabPage->acPreFileContext , pnodeTabPage->nPreFileContextLen );
		len = pnodeTabPage->nPreFileContextLen ;

		pnodeTabPage->bNeedWrotePreFileContext = FALSE ;

		return len;
	}

	len = size * nmemb ;
	if( pnodeTabPage->nWroteLen + len > pnodeTabPage->nFileSize )
		len = pnodeTabPage->nFileSize - pnodeTabPage->nWroteLen ;

	if( pnodeTabPage->bHexEditMode == FALSE )
	{
		GetTextByRange( pnodeTabPage , (int)(pnodeTabPage->nWroteLen) , pnodeTabPage->nWroteLen+len , (char*)buffer ) ;
	}
	else
	{
		memcpy( buffer , pnodeTabPage->acWriteFileBuffer + pnodeTabPage->nWroteLen , len );
	}

	pnodeTabPage->nWroteLen += len ;

	return len;
}

int OnSaveFile( struct TabPage *pnodeTabPage , BOOL bSaveAs )
{
	char		acFullPathFilename[ MAX_PATH ] ;
	char		acFilename[ MAX_PATH ] ;
	char		acExtname[ MAX_PATH ] ;

	int		nret = 0;

	if( pnodeTabPage->pScintilla == NULL )
		return -1;

	if( IsDocumentModified(pnodeTabPage) == false && bSaveAs != TRUE )
		return -2;
	
	if( pnodeTabPage->stRemoteFileServer.acNetworkAddress[0] )
	{
		CURL			*curl = NULL ;
		CURLcode		res ;
		char			url[ 1024 ] ;
		char			userpwd[ 256 ] ;

		if( pnodeTabPage->bHexEditMode == FALSE )
		{
			pnodeTabPage->nFileSize = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETLENGTH, 0, 0);
		}
		else
		{
			pnodeTabPage->acWriteFileBuffer = (char*)StrdupHexEditTextFold( pnodeTabPage , & pnodeTabPage->nFileSize ) ;
			if( pnodeTabPage->acWriteFileBuffer == NULL )
				return -3;
		}

		pnodeTabPage->nWroteLen = 0 ;
		if( pnodeTabPage->nPreFileContextLen > 0 )
			pnodeTabPage->bNeedWrotePreFileContext = TRUE ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			free( pnodeTabPage->acWriteFileBuffer ); pnodeTabPage->acWriteFileBuffer = NULL ;
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		strncpy( url , pnodeTabPage->acPathFilename , sizeof(url)-1 );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , pnodeTabPage->stRemoteFileServer.acLoginUser , pnodeTabPage->stRemoteFileServer.acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		curl_easy_setopt( curl , CURLOPT_UPLOAD , 1L );
		curl_easy_setopt( curl , CURLOPT_READFUNCTION , WriteRemoteFile );
		curl_easy_setopt( curl , CURLOPT_READDATA , pnodeTabPage );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( pnodeTabPage->bHexEditMode == FALSE )
		{
			;
		}
		else
		{
			free( pnodeTabPage->acWriteFileBuffer ); pnodeTabPage->acWriteFileBuffer = NULL ;
		}
		if( res != CURLE_OK )
		{
			::MessageBox(NULL, TEXT("连接远程文件服务器失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
			curl_easy_cleanup( curl );
			return -3;
		}
		else
		{
			curl_easy_cleanup( curl );
		}
	}
	else if( pnodeTabPage->acPathFilename[0] == '\0' || bSaveAs == TRUE )
	{
		nret = GetFileNameBySaveFileDialog( acFullPathFilename, sizeof(acFullPathFilename));
		if (nret)
			return -3;

		SplitPathFilename( acFullPathFilename , NULL , NULL , acFilename , NULL , acExtname , NULL , NULL );

		nret = DoWriteFile( pnodeTabPage, acFullPathFilename);
		if (nret)
			return -4;

		strcpy( pnodeTabPage->acPathFilename, acFullPathFilename );
		strcpy( pnodeTabPage->acFilename, acFilename );
		pnodeTabPage->nFilenameLen = strlen(pnodeTabPage->acFilename) ;
		SetWindowTitle( pnodeTabPage->acPathFilename );

		pnodeTabPage->pstDocTypeConfig = GetDocTypeConfig( acExtname ) ;

		InitTabPageControlsBeforeLoadFile( pnodeTabPage );

		InitTabPageControlsAfterLoadFile( pnodeTabPage );

		PushOpenPathFilenameRecently( pnodeTabPage->acPathFilename );
	}
	else
	{
		nret = DoWriteFile( pnodeTabPage, pnodeTabPage->acPathFilename );
		if( nret )
			return -5;
	}

	struct stat	statbuf ;
	stat( pnodeTabPage->acPathFilename , & statbuf );
	pnodeTabPage->st_mtime = statbuf.st_mtime ;

	OnSavePointReached( pnodeTabPage );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETSAVEPOINT, 0, 0) ;

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	// 重新计算各窗口大小
	UpdateAllWindows( g_hwndMainWindow );

	if( pnodeTabPage && pnodeTabPage->pstDocTypeConfig && pnodeTabPage->pstDocTypeConfig->pfuncParseFileConfigHeader )
	{
		nret = pnodeTabPage->pstDocTypeConfig->pfuncParseFileConfigHeader( pnodeTabPage ) ;
		if( nret )
			return nret;
	}

	if( g_stEditUltraMainConfig.nReloadSymbolListOrTreeInterval > 0 )
	{
		BeginReloadSymbolListOrTreeThread();
	}

	return 0;
}

int OnSaveFileAs( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage == NULL )
		return 0;

	if ( pnodeTabPage->pScintilla == NULL)
		return -1;

	OnSaveFile( pnodeTabPage , TRUE );

	return 0;
}

int OnSaveAllFiles()
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*pnodeTabPage = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0 ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		pnodeTabPage = (struct TabPage *)(tci.lParam);
		OnSaveFile( pnodeTabPage , FALSE ) ;
	}

	return 0;
}

int OnCloseFile( struct TabPage *pnodeTabPage )
{
	if( IsDocumentModified(pnodeTabPage) == true )
	{
		char	msg[100 + MAX_PATH];
		int	decision;
		_snprintf(msg, sizeof(msg)-1, "保存到[%s] ?", pnodeTabPage->acPathFilename);
		decision = MessageBox(g_hwndMainWindow, msg, g_acAppName, MB_YESNOCANCEL);
		if (decision == IDCANCEL)
			return -1;
		else if (decision == IDYES)
			OnSaveFile( pnodeTabPage , FALSE );
	}

	CleanTabPageControls( pnodeTabPage );

	/* 如果设置了打开文件后锁定文件 */
	if( g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile == TRUE )
	{
		DWORD dwFileAttr = ::GetFileAttributes( pnodeTabPage->acPathFilename ) ;
		dwFileAttr &= ~FILE_ATTRIBUTE_READONLY ;
		::SetFileAttributes( pnodeTabPage->acPathFilename , dwFileAttr );
	}

	/* 清理该文件的所有导航退回结构 */
	CleanNavigateBackNextTraceListByThisFile( pnodeTabPage );

	RemoveTabPage( pnodeTabPage );

	// 重新计算各窗口大小
	CalcTabPagesHeight();
	/*
	AdjustTabPages();
	AdjustTabPageBox( pnodeTabPage );
	*/
	UpdateAllMenus( g_hwndMainWindow , g_pnodeCurrentTabPage );
	UpdateAllWindows( g_hwndMainWindow );

	return 0;
}

int OnCloseAllFile()
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	int		nThisTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*pnodeTabPage = NULL ;
	int		nret = 0 ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	nThisTabPageIndex = 0 ;
	for( nTabPageIndex = 0 ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nThisTabPageIndex , & tci );
		pnodeTabPage = (struct TabPage *)(tci.lParam);
		nret = OnCloseFile( pnodeTabPage ) ;
		if( nret == -1 )
			nThisTabPageIndex++;
	}

	return 0;
}

int OnCloseAllExpectCurrentFile( struct TabPage *pnodeTabPage )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	int		nThisTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;
	int		nret = 0 ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	nThisTabPageIndex = 0 ;
	for( nTabPageIndex = 0 ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nThisTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		if( p == pnodeTabPage )
		{
			nThisTabPageIndex++;
			continue;
		}
		nret = OnCloseFile( p ) ;
		if( nret == -1 )
			nThisTabPageIndex++;
	}

	return 0;
}

int OnEnableCreateNewFileOnNewBoot( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bCreateNewFileOnNewBoot == FALSE )
	{
		g_stEditUltraMainConfig.bCreateNewFileOnNewBoot = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bCreateNewFileOnNewBoot = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	return 0;
}

int OnOpenFilesThatOpenningOnExit( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bOpenFilesThatOpenningOnExit == FALSE )
	{
		g_stEditUltraMainConfig.bOpenFilesThatOpenningOnExit = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bOpenFilesThatOpenningOnExit = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	return 0;
}

int OnSetReadOnlyAfterOpenFile( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile == FALSE )
	{
		g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	return 0;
}

int FillAllOpenFilesOnBoot()
{
	memset( g_stEditUltraMainConfig.aacOpenFilesOnBoot , 0x00 , sizeof(g_stEditUltraMainConfig.aacOpenFilesOnBoot) );
	int count = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	if( count > OPENFILES_ON_BOOT_MAXCOUNT )
		count = OPENFILES_ON_BOOT_MAXCOUNT ;
	for( int index = 0 ; index < count ; index++ )
	{
		struct TabPage	*p = GetTabPageByIndex( index ) ;

		strncpy( g_stEditUltraMainConfig.aacOpenFilesOnBoot[index] , p->acPathFilename , sizeof(g_stEditUltraMainConfig.aacOpenFilesOnBoot[index])-1 );
	}

	return 0;
}

int CheckAllFilesSave()
{
	int		nTabPagesCount ;
	TCITEM		tci ;
	struct TabPage	*pnodeTabPage = NULL ;
	int		nret = 0 ;

	while(1)
	{
		nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
		if( nTabPagesCount == 0 )
			break;

		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , 0 , & tci );
		pnodeTabPage = (struct TabPage *)(tci.lParam);
		nret = OnCloseFile( pnodeTabPage ) ;
		if( nret )
			return -1;
	}

	return 0;
}

int OnFileCheckUpdateWhereSelectTabPage( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bCheckUpdateWhereSelectTabPage == FALSE )
	{
		g_stEditUltraMainConfig.bCheckUpdateWhereSelectTabPage = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bCheckUpdateWhereSelectTabPage = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	return 0;
}

int OnFilePromptWhereAutoUpdate( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bPromptWhereAutoUpdate == FALSE )
	{
		g_stEditUltraMainConfig.bPromptWhereAutoUpdate = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bPromptWhereAutoUpdate = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	return 0;
}

int OnFileSetNewFileEols( struct TabPage *pnodeTabPage , int nNewFileEolsMode )
{
	g_stEditUltraMainConfig.nNewFileEols = nNewFileEolsMode ;

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	return 0;
}

int OnFileConvertEols( struct TabPage *pnodeTabPage , int nConvertEolsMode )
{
	if( pnodeTabPage == NULL )
		return 0;

	if( pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETEOLMODE,0,0) != nConvertEolsMode )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_CONVERTEOLS, nConvertEolsMode, 0 );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_SETEOLMODE, nConvertEolsMode, 0 );

		UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

		SaveMainConfigFile();
	}

	return 0;
}

int OnFileSetNewFileEncoding( struct TabPage *pnodeTabPage , int nNewFileEncoding )
{
	g_stEditUltraMainConfig.nNewFileEncoding = nNewFileEncoding ;

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	return 0;
}

int OnFileConvertEncoding( struct TabPage *pnodeTabPage , int nConvertEncoding )
{
	char		*pcret = NULL ;

	if( pnodeTabPage == NULL )
		return 0;

	if( pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETCODEPAGE,0,0) != nConvertEncoding )
	{
		int		nCurrentPos ;
		int		nCurrentLine ;

		size_t		nFileSize ;
		char		*acFileBuffer = NULL ;
		char		*pcInput = NULL ;
		size_t		nConvertSize ;
		char		*acConvertBuffer = NULL ;

		nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;

		nFileSize = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
		acFileBuffer = (char*)malloc( nFileSize + 1 ) ;
		if( acFileBuffer == NULL )
			return -1;
		memset( acFileBuffer , 0x00 , nFileSize + 1 );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXT , (sptr_t)(nFileSize+1) , (sptr_t)acFileBuffer ) ;

		nConvertSize = 3 + nFileSize * 3 ;
		acConvertBuffer = (char*)malloc( nConvertSize + 1 ) ;
		if( acConvertBuffer == NULL )
		{
			free( acFileBuffer );
			return -1;
		}
		memset( acConvertBuffer , 0x00 , nConvertSize + 1 );

		/*
		if( pnodeTabPage->nCodePage == 65001 )
		{
			pcInput = acFileBuffer + 3 ;
			nFileSize -= 3 ;
		}
		else
		{
		*/
			pcInput = acFileBuffer ;
		/*
		}
		*/
		pcret = ConvertEncodingEx( GetEncodingString(pnodeTabPage->nCodePage) , GetEncodingString(nConvertEncoding) , pcInput , nFileSize , acConvertBuffer , & nConvertSize ) ;
		if( pcret == NULL )
		{
			free( acFileBuffer );
			free( acConvertBuffer );
			ErrorBox( "全文转换字符编码，从[%s]到[%s]，失败" , GetEncodingString(pnodeTabPage->nCodePage) , GetEncodingString(nConvertEncoding) );
			return -1;
		}

		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_CLEARALL, 0, 0 );

		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_SETCODEPAGE, nConvertEncoding, 0 );
		pnodeTabPage->nCodePage = nConvertEncoding ;
		if( nConvertEncoding == ENCODING_UTF8 )
			g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_ADDTEXT, 3, (sptr_t)"\xEF\xBB\xBF" );
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_ADDTEXT, nConvertSize, (sptr_t)acConvertBuffer );

		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GOTOLINE, nCurrentLine, 0 ) ;

		if( nConvertEncoding == ENCODING_GBK || nConvertEncoding == ENCODING_BIG5 )
			g_pnodeCurrentTabPage->nPreFileContextLen = 0 ;

		free( acFileBuffer );
		free( acConvertBuffer );

		UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

		SaveMainConfigFile();
	}

	return 0;
}

