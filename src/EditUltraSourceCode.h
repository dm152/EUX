#ifndef _H_EDITULTRA_VIEW_
#define _H_EDITULTRA_VIEW_

#include "framework.h"

int OnSourceCodeBlockFoldVisiable( struct TabPage *pnodeTabPage );
int OnSourceCodeBlockFoldToggle( struct TabPage *pnodeTabPage );
int OnSourceCodeBlockFoldContract( struct TabPage *pnodeTabPage );
int OnSourceCodeBlockFoldExpand( struct TabPage *pnodeTabPage );
int OnSourceCodeBlockFoldContractAll( struct TabPage *pnodeTabPage );
int OnSourceCodeBlockFoldExpandAll( struct TabPage *pnodeTabPage );

int OnSetReloadSymbolListOrTreeInterval( struct TabPage *pnodeTabPage );

int OnSourceCodeEnableAutoCompletedShow( struct TabPage *pnodeTabPage );
int OnSourceCodeAutoCompletedShowAfterInputCharacters( struct TabPage *pnodeTabPage );
int OnSourceCodeEnableCallTipShow( struct TabPage *pnodeTabPage );

#define	BEGIN_DATABASE_CONNECTION_CONFIG	"-- EDITULTRA BEGIN DATABASE CONNECTION CONFIG"
#define END_DATABASE_CONNECTION_CONFIG		"-- EDITULTRA END DATABASE CONNECTION CONFIG"

int OnInsertDataBaseConnectionConfig( struct TabPage *pnodeTabPage );

#define	BEGIN_REDIS_CONNECTION_CONFIG	"-- EDITULTRA BEGIN REDIS CONNECTION CONFIG"
#define END_REDIS_CONNECTION_CONFIG	"-- EDITULTRA END REDIS CONNECTION CONFIG"

int OnInsertRedisConnectionConfig( struct TabPage *pnodeTabPage );

#endif
